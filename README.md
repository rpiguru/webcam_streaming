# Video streaming from RPi
==================================
## Streaming from Raspicam

### Using `uv4l`
    
    curl http://www.linux-projects.org/listing/uv4l_repo/lrkey.asc | sudo apt-key add -
    
    sudo nano /etc/apt/sources.list
    
  Add this:
    
    deb http://www.linux-projects.org/listing/uv4l_repo/raspbian/ jessie main
    
  Now, let us install
    
    sudo apt-get update
    sudo apt-get install uv4l uv4l-raspicam
    sudo apt-get install uv4l-raspicam-extras
    
    sudo apt-get install uv4l-webrtc
    
Now, change the port of `uv4l` streaming.
    
    sudo nano /etc/uv4l/uv4l-raspicam.conf
    
  And change like this:
    
    server-option = --port=5544

Restart server

    sudo service uv4l_raspicam restart
    
Open the browser and visit `http://<RPi's IP>:5544/stream`
    
Open the vlc player and set stream address like this:
    
    http://raspberrypi:5544/stream/video.mjpeg
    

# Via webcam

## Install ffmpeg

	sudo apt-get update
	cd /usr/src
	sudo git clone git://git.videolan.org/x264
	cd x264
	sudo ./configure --host=arm-unknown-linux-gnueabi --enable-static --disable-opencl
	sudo make
	sudo make install

	cd /usr/src
	sudo git clone https://github.com/FFmpeg/FFmpeg.git
	cd ffmpeg
	sudo ./configure --arch=armel --target-os=linux --enable-gpl --enable-libx264 --enable-nonfree
	sudo make
	sudo make install

## Install web streaming server
	
	cd ~
	sudo apt-get install libv4l-dev
	sudo apt-get install libjpeg8-dev
	sudo apt-get install subversion
	sudo apt-get install imagemagick
	svn co https://svn.code.sf.net/p/mjpg-streamer/code/
	cd /home/pi/code/mjpg-streamer/
	make USE_LIBV4L2=true clean all
	sudo make DESTDIR=/usr install

Start server
	
	mjpg_streamer -i "/usr/lib/input_uvc.so -d /dev/video0 -y -r 640x480 -f 10" -o "/usr/lib/output_http.so -p 8090 -w /var/www/mjpg_streamer"

## Build WiFi AP.
	
- Install packages.

        sudo apt-get install dnsmasq hostapd

- CONFIGURE YOUR INTERFACES

    The first thing you'll need to do is to configure your `wlan0` interface with a static IP.

    In newer Raspian versions, interface configuration is handled by `dhcpcd` by default. 
    We need to tell it to ignore `wlan0`, as we will be configuring it with a static IP address elsewhere. 

    So open up the `dhcpcd` configuration file with `sudo nano /etc/dhcpcd.conf` and add the following line to the bottom of the file:

        denyinterfaces wlan0  
    
    Note: This must be ABOVE any interface lines you may have added!

    Now we need to configure our static IP. 

    To do this open up the interface configuration file with `sudo nano /etc/network/interfaces` and edit the `wlan0` section so that it looks like this:

        allow-hotplug wlan0  
        iface wlan0 inet static  
            address 172.24.1.1
            netmask 255.255.255.0
            network 172.24.1.0
            broadcast 172.24.1.255
    
    Restart `dhcpcd` with `sudo service dhcpcd restart` and then reload the configuration for `wlan0` with `sudo ifdown wlan0; sudo ifup wlan0`.

- CONFIGURE HOSTAPD
    
    Next, we need to configure `hostapd`. 
    
    Create a new configuration file with `sudo nano /etc/hostapd/hostapd.conf` with the following contents:

        # This is the name of the WiFi interface we configured above
        interface=wlan0
        
        # Use the nl80211 driver with the brcmfmac driver
        driver=nl80211
        
        # This is the name of the network
        ssid=VLscope
        
        # Use the 2.4GHz band
        hw_mode=g
        
        # Use channel 6
        channel=6
        
        # Enable 802.11n
        ieee80211n=1
        
        # Enable WMM
        wmm_enabled=1
        
        # Enable 40MHz channels with 20ns guard interval
        ht_capab=[HT40][SHORT-GI-20][DSSS_CCK-40]
        
        # Accept all MAC addresses
        macaddr_acl=0
        
        # Use WPA authentication
        auth_algs=1
        
        # Require clients to know the network name
        ignore_broadcast_ssid=0
        
        # Use WPA2
        wpa=2
        
        # Use a pre-shared key
        wpa_key_mgmt=WPA-PSK
        
        # The network passphrase
        wpa_passphrase=VL_scope
        
        # Use AES, instead of TKIP
        rsn_pairwise=CCMP
    
    NOTE: We assigned **SSID & PASSWORD** as `VLscope & VL_scope` and these values will be embedded into the firmware of ESP.
        
    We can check if it's working at this stage by running `sudo /usr/sbin/hostapd /etc/hostapd/hostapd.conf`. 
    
    If it's all gone well thus far, you should be able to see to the network **VLscope**! 
    
    If you try connecting to it, you will see some output from the Pi, but you won't receive and IP address until we set up `dnsmasq` in the next step. 
    
    Use **Ctrl+C** to stop it.

    We aren't quite done yet, because we also need to tell `hostapd` where to look for the config file when it starts up on boot. 
    
    Open up the default configuration file with `sudo nano /etc/default/hostapd` and find the line `#DAEMON_CONF=""` and replace it with `DAEMON_CONF="/etc/hostapd/hostapd.conf"`.
    
- CONFIGURE DNSMASQ
    
    The shipped `dnsmasq` config file contains a wealth of information on how to use it, but the majority of it is largely redundant for our purposes. 
    
    I'd advise moving it (rather than deleting it), and creating a new one with

        sudo mv /etc/dnsmasq.conf /etc/dnsmasq.conf.orig  
        sudo nano /etc/dnsmasq.conf  
    
    Paste the following into the new file:

        interface=wlan0                 # Use interface wlan0  
        listen-address=172.24.1.1       # Explicitly specify the address to listen on  
        bind-interfaces                 # Bind to the interface to make sure we aren't sending things elsewhere  
        server=8.8.8.8                  # Forward DNS requests to Google DNS  
        domain-needed                   # Don't forward short names  
        bogus-priv                      # Never forward addresses in the non-routed address spaces.  
        dhcp-range=172.24.1.50,172.24.1.150,12h # Assign IP addresses between 172.24.1.50 and 172.24.1.150 with a 12 hour lease time  
        
    You can check with your mobile by trying to access to the RPi3's AP. (VLscope & VL_scope)
    
## Test webcam streaming.

Connect your iPhone to the RPi's AP.

Open the **Google Chrome** and visit this:
	
	http://172.24.1.1:8090/?action=stream

## Enable auto-starting
	
Open the `/etc/rc.local` file.
	
	sudo nano /etc/rc.local

Add this line before `exit 0`
	
	(/usr/bin/mjpg_streamer -i "/usr/lib/input_uvc.so -d /dev/video0 -y -r 640x480 -f 10" -o "/usr/lib/output_http.so -p 8090 -w /var/www/mjpg_streamer")&

## REFERENCE

http://www.jeffreythompson.org/blog/2014/11/13/installing-ffmpeg-for-raspberry-pi/
http://diyhacking.com/raspberry-pi-webcam-robot/